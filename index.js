import fileUpload from "express-fileupload"

import express from "express"
import usersRouter from "./routers/users.js";
import authRouter from "./routers/auth.js";
import filesRouter from "./routers/files.js";
import { expressjwt} from "express-jwt";

const app=express();

app.use(fileUpload({
    useTempFiles : true,
    createParentPath: true
}));

app.use(
    expressjwt({
      secret: process.env.SECRET || "SECRET",
      algorithms: ["HS256"],
    }).unless({ path: ['/api/login',/\.[0-9a-z]+$/i,/(\/api\/login\/[0-9a-z]+$)/i] })
);

app.use(function (err, req, res, next) {
    if (err.name === "UnauthorizedError") {
        res.status(401).json({message:'Token is invalid'});
    } else {
        next(err);
    }
});

app.use(usersRouter);
app.use(authRouter);
app.use(filesRouter);
// app.post('/upload',(req,res)=>{
//     console.log(req.body['2'],req.files.file);
//     res.json({message:"ok"})
// })


app.listen(parseInt(process.env.PORT) || 3000,async ()=>{
})