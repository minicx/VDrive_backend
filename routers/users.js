import { Router } from "express";
import userController from '../controllers/users.js'

const usersRouter=new Router();


usersRouter.get('/api/users/me',userController.getCurrentUser)
usersRouter.get('/api/users',userController.getUsers); // get users
usersRouter.get('/api/users/:mail',userController.getUser); // get user by mail

usersRouter.post('/api/users',userController.addUser); // add new user

usersRouter.put('/api/users/:mail',userController.updateUser); // update user by mail

usersRouter.delete('/api/users/:mail',userController.deleteUser); // delete user by mail

export default usersRouter