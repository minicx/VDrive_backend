import { Router } from "express";
import authController from "../controllers/auth.js";
const authRouter=new Router();



authRouter.post('/api/login',authController.login);

authRouter.post('/api/login/checkToken',authController.checkToken);

export default authRouter