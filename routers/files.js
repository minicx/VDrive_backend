import { Router } from "express";
import filesController from "../controllers/files.js"

const filesRouter=new Router();


filesRouter.get('/api/files',filesController.getFiles);
filesRouter.post('/api/files',filesController.addFile);
filesRouter.get('/api/file',filesController.getFile);

filesRouter.get('/api/directories',filesController.getDirectories);
filesRouter.post('/api/directories',filesController.addDirectory)

export default filesRouter