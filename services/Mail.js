
import nodemailer from 'nodemailer'

import {config} from "dotenv"
config();


const INFOTemplate=(login,password,url)=>{ return `Здраствуйте, вам создали аккаунт на информационной площадке!
Ваш логин: ${login}
Ваш пароль: ${password}
Ссылка на авторизацию на сайте: ${url}
` // информация о входе
}



class MailProvider{
    transporter;
    constructor(){
        
        this.transporter = nodemailer.createTransport({
            host: process.env.SMTPSERVER || 'none',
            port: process.env.SMTPPORT || 300,
            secure: true,
            auth: {
              user: process.env.MAILLOGIN,
              pass: process.env.MAILPASSWORD,
            },
        });
    }
    async sendINFO(mail,password){
        try {
            await this.transporter.sendMail({
                from: `Notifier <${process.env.MAILLOGIN || 'none'}>`, // sender address
                to: mail, // list of receivers
                subject: "Регистрация на информационной площадке...", // Subject line
                text: INFOTemplate(mail,password,process.env.URLSERVICE), // plain text body
                // html: "<b>Hello world?</b>", // html body
              });
            return true
        } catch (err){
            return false
        }
        
    }
}


export const mail= new MailProvider();