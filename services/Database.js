import { JsonDB , Config } from "node-json-db";
import  bcrypt from "bcryptjs";


export class Database {
    db=new JsonDB(new Config('db',true,true,'/'));
    constructor (){
        
        let DEFAULT={
            accounts:[],
            files:[],
            directories:[],
        }
        bcrypt.hash(process.env.ADMINPASSWORD || "admin", 5).then((hash)=>{
            DEFAULT.accounts.push({
                mail: process.env.ADMINLOGIN || "ADMIN@ADMIN.ORG",
                password: hash,
                firstName: 'admin',
                lastName: null,
                middleName: '',
                isAdmin: true,
                department: '',
                birthday: null,
                accessGranted: true
            })
            
            this.db.getData('/').then((result)=>{
                if (Object.keys(result).length == 0){
                    this.db.push('/',DEFAULT)
                }
            }) 
        })
         
    }
    
    async getUsers(){
        return await this.db.getObject('/accounts')
    }
    async getUser(mail){
        const index=await this.db.getIndex("/accounts", mail, "mail");
        if (index==-1){
            return null
        }
        return await this.db.getData(`/accounts[${index}]`)
    }
    async getDirectories(){
        return await this.db.getObject('/directories')
    }
    async getFiles(){
        return await this.db.getObject('/files')
    }
    async addFile(object){
        this.db.push('/files[]',object)
    }
    async addDirectory(object){
        this.db.push('/directories[]',object)
    }
} 
export const db=new Database()