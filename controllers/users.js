import { db } from "../services/Database.js"
import  bcrypt from "bcryptjs";

class UsersController {
    
    async getCurrentUser(req,res) {
        const user=await db.getUser(req.auth.mail);
        if (user!=null){
            res.status(200).json(user)
        } else {
            res.status(500).json({message:'Internal error'})
        }
        
    }
    
    async getUsers (req,res){
        res.json({users:await db.getUsers()});
    }
    async getUser (req,res) {
        if (req.params['mail']){
            const user=await db.getUser(req.params['mail']);
            if (user==null){
                res.status(404).json({message:'User not found'})
            } else {
                res.json(user);
            }
        } else {
            res.status(400).json({message:'Mail isnt provided'})
        }
        
        
    }
    async addUser (req,res){
        
        
        // await bcrypt.compare("B4c0/\/", hash);
        // await bcrypt.hash("B4c0/\/", 5)
    }
    async updateUser (req,res){

    }
    async deleteUser (req,res){

    }
}

export default new UsersController()