import jwt from "jsonwebtoken";
import { db } from "../services/Database.js";
import  bcrypt from "bcryptjs";
import { mail } from "../services/Mail.js";
class AuthController {
    async checkToken(req,res){
        if (req.body!==undefined){
            if (req.body['token']!==undefined){
                try {
                    jwt.verify(req.body['token'],process.env.SECRET || "SECRET");
                    res.status(200).json({status:'pass'});
                } catch (err){
                    res.status(500).json({message:'Bad token'});
                }
                
            }else {
                res.status(400).json({message:'No token'})
            }
        } else {
            res.status(500).json({message:'No body'})
        }
    }
    async login(req,res){
        // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoic2FzYSIsImlhdCI6MTY3MTIzMzcyNiwiZXhwIjoxNjcxMjM3MzI2fQ.tPBDKhn2QVFNFWaVcTB1xVmLVq__e6cCPwuUHs4O_nU
        /* 
        req.auth
        {
            data: "sasa",
            iat: 1671233726,
            exp: 1671237326,
        }
        */
        if (req.body!==undefined){
            if (req.body['mail']!=null && req.body['password']!=null){
                const isMail=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(req.body['mail']);
                if (isMail){
    
                    const user=await db.getUser(req.body.mail);
                    if (user==null){
                        res.status(404).json({message:'Cant find any user'})
                    } else{
                        //const hashPassword=await bcrypt.hash(, 5)
                        if (await bcrypt.compare(req.body.password, user.password) && isMail){
                            const token=jwt.sign({mail:req.body['mail'],isAdmin:user.isAdmin}, process.env.SECRET || "SECRET", { expiresIn: `24h` });
                            res.json({token:token})
                        } else {
                            res.status(400).json({message:'Wrong password or mail'})
                        }
                    }
                }else {
                    res.status(400).json({message:'Wrong mail'})
                }
                
                
            } else {
                res.status(400).json({message:'Wrong password or mail'})
            }
        } else {
            res.status(500).json({message:'No body'})
        }
        
        
    }

}

export default new AuthController();