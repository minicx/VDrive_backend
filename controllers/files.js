import path from "path"
import fs from "fs"
import { db } from "../services/Database.js"
class FilesController {
    async getDirectories(req,res){
        res.json(await db.getDirectories())
    }
    async getFiles(req,res){
        res.json(await db.getFiles())
    }
    async addDirectory(req,res){
        if (req.body.name){
            const directories=await db.getDirectories()
            const isAdded= directories.filter((directory)=>{
                if (req.body.name.indexof(directory.name)!=-1){
                    return true
                } else {
                    return false
                }
            }).length>0 ? true : false
            if (isAdded){
                res.status(400).json({message:'Already added'})
            } else {
                const now=+(new Date())
            
                const newId= directories.length>0 ? directories[directories.length-1].id+1 : 0
                await db.addDirectory({
                    id: newId,
                    name: req.body.name,
                    creationDate: now
                })
                res.status(200).json({message:'ok'})
            }
            
        } else {
            res.status(400).json({message: "Wrong name"})
        }
    }
    async addFile(req,res){
        
        if (req.files.file && req.auth.isAdmin){
            const now=+(new Date())
            db.getFiles().then((files)=>{
                const newId=files.length>0 ? files[files.length-1].id+1 : 0
                req.files.file.mv(path.resolve("data",`${req.body.rootDirectory}_${newId}_${req.body.name}`) ,function(err) {
                    if (err){
                        return res.status(500).send({message:err});

                    }
                    
                    db.addFile({
                        id : newId,
                        name: req.body.name,
                        rootDirectory: req.body.rootDirectory,
                        needAccess: req.body.needAccess,
                        uploadDate: now
                    }).then (()=>{
                        res.status(200).json({messsage: 'ok'})
                    }).catch(()=>{
                        res.status(500).json()
                    })
                })
            })
            
            
        } else {
            res.status(400).json({message:'Cant find any file or doesnt have any access to this method'})
        }
        
        console.log(req.body['2'],req.files.file);
    }
    async getFile(req,res){
        try{
            const file=(await db.getFiles()).filter((file)=>{
                if (file.id==req.body.id){
                    return true
                } else {
                    return false
                }
            })[0]
        } catch( err){
            res.status(500).json({message: err})
        }
        
        const filePath=path.resolve('data',`${file.rootDirectory}_${file.id}_${file.name}`)
        res.sendFile(filePath,(err)=>{
            res.status(500).json({message: err})
        })
    }
}

export default new FilesController()